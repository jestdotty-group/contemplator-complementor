//usage: importAll(require.context(folder, true, regex))
export default (context)=>{
	return context
		.keys()
		.reduce((o, key)=>{
			o[key] = context(key)
			return o
		}, {})
}
